<?php
//genereer formulier =====================================================================

require_once "modules/syst/ProjectIni.class.php";
$formid=time();

if(isset($_GET['tabel']))		{$tabelnaam 	= ($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX.$_GET['tabel'];}
if(isset($_GET['velden']))		{$velden		= explode(",",$_GET['velden']);}
if(isset($_GET['types']))		{$types			= explode(",",$_GET['types']);}
if(isset($_GET['lengten']))		{$veldlengten	= explode(",",$_GET['lengten']);}
if(isset($_GET['schermnamen']))	{$schermnaam	= explode(",",$_GET['schermnamen']);}
if(isset($_GET['schermtypen']))	{$schermtypen	= explode(",",$_GET['schermtypen']);}

//genereer database ======================================================================

$Qcreate="CREATE TABLE IF NOT EXISTS `".$tabelnaam."` (`".$tabelnaam."Rec` INT(7) AUTO_INCREMENT PRIMARY KEY,`formid` INT(10),";
for($V=0;$V<sizeof($velden);$V++)
	{
	$Qcreate.="`".$velden[$V]."` ".$types[$V];
	if($types[$V]!="DATETIME"){$Qcreate.="(".$veldlengten[$V]."), ";}else{$Qcreate.=", ";}
	}
$Qcreate=substr($Qcreate, 0, -2).");";
//echo("<br>");echo($Qcreate);echo("<br>");

// handmatig doen: // $Xcreate = $db->exec($Qcreate); 

//genereer invoerform ==================================================================
$display="";
//$display.="<form method=\"post\"><input type=\"text\" name=\"formid\" value=\"".$formid."\">"; // werkt op zich goed
$display.="<form method=\"post\">formid<input type=\"text\" name=\"formid\" value=\"<"."?php "."echo(time()); "."?".">\">";
foreach($schermnaam as $key => $val)
{
$display.=$val."<input type=\"".$schermtypen[$key]."\" name=\"".$val."\" value=\"".$val."\">";

}
//$display.="<input name=\"creeertabel\" type=\"submit\" value=\"maak\"></form>";
$display.="<input type=\"submit\" value=\"maak\"></form>";

// genereer action script ================================================================

$actionscript    =	"<h1>oo php + mysql action script</h1><h2>Copy and embed into your php action script</h2>";
$OOPactionscript =	$actionscript;

$actionscript1    = "\n<br/>\n<br/>if(isset(\$_POST['formid']) && ";
$OOPactionscript1 = "\n<h3>Create(\$formid) method snippets</h3>"."\nif(isset(\$formid) && ";
foreach($schermnaam as $key => $val)
	{
	$actionscript1   .=	"isset(\$_POST['".$val."']) && ";	
	$OOPactionscript1.=	"\$this->get".ucfirst($val)."() !== NULL && ";
	}
$actionscript1   .=	"TRUE){\n<br/>";
$OOPactionscript1.=	"TRUE)\n<br/>{\n<br/>";

$actionscript    .=	$actionscript1;
$OOPactionscript .=	$OOPactionscript1;

$actionscript2 =	"\$vh=\$db->exec(\"DELETE FROM  `".$tabelnaam."` WHERE  `formid` = '\".\$_POST['formid'].\"' \");";
$OOPactionscript2 =	"\n\t\$instal = \$DB->exec(\"".$Qcreate."\");\n<br/>\t\$redundant = \$this->Delete(\" WHERE  `formid` = '\".\$formid.\"' \");";

$actionscript2 .=	"\n<br/>\$laatste=\$db->exec(\"INSERT INTO `".$tabelnaam."` (`formid`, `";
$OOPactionscript2.=	"\n<br/>\t\$nieuw = \$DB->exec(\"INSERT INTO `".$tabelnaam."` (`formid`, `";

foreach($velden as $key=>$val)
{
	$actionscript2 .=	$val. "` , `";
	$OOPactionscript2.= $val. "` , `";
} 
$actionscript2 = 	substr($actionscript2, 		0, -3);
$OOPactionscript2 = substr($OOPactionscript2, 	0, -3);

$actionscript2  .=	") VALUES ('\".\$_POST['formid'].\"', '";
$OOPactionscript2.=	") VALUES ('\".\$formid.\"', '";

foreach($schermnaam as $key=>$val)
{
	$actionscript2 .=	"\".\$_POST['".$val. "'].\"' , '";
	$OOPactionscript2.=	"\".\$this->get". ucfirst($val). "().\"' , '";
} 
$actionscript2 = 	substr($actionscript2, 0, -3).");\"";
$OOPactionscript2 = substr($OOPactionscript2, 0, -3).")\"";

$actionscript2 .=	");";
$OOPactionscript2.=	");";

$actionscript .=	$actionscript2."\n<br/>}";
$OOPactionscript .=	$OOPactionscript2."\n<br/>}";

$OOPactionscriptCreate = $OOPactionscript;
$OOPactionscriptRetrieve = "\n<h3>Retrieve(\$parameterArr, \$WHEREcondition) method snippets</h3>\$DB->query('SELECT '.implode(\",\",\$parameterArr).' FROM `".$tabelnaam."` '.\$WHEREcondition);";

$OOPactionscriptUpdate = "\n<h3>Update(\$parameterArr, \$waardenArr, \$WHEREcondition) method snippets</h3>
			\$XUpdate = \"UPDATE `".$tabelnaam."` SET 
			\$PP = sizeof(\$parameterArr)\"; 
\n<br/>	if(\$PP == sizeof(\$waardenArr))
\n<br/>	{
\n<br/>		for(\$P=0; \$P<\$PP; \$P++)
\n<br/>		{
\n<br/>			\$XUpdate .= \"`\" . \$parameterArr[\$P] . \"` = '\" . \$waardenArr[\$P] . \"',\";
\n<br/>		}
\n<br/>		\$XUpdate = substr(\$XUpdate,0,-1); // remove latest comma
\n<br/>		\$XUpdate.= \" \".\$WHEREcondition.\" \";
\n<br/>		\$fris = \$DB->exec(\$XUpdate); 
\n<br/>		}
\n<br/>		else
\n<br/>		{
\n<br/>			return \"Mismatch in \".__CLASS__.\" \".__METHOD__;
\n<br/>		}";

$OOPactionscriptDelete = "\n<h3>Delete(\$WHEREcondition) method snippets</h3>\$DB->exec(\"DELETE FROM `".$tabelnaam."` \".\$WHEREcondition);";

$OOPactionscript = $OOPactionscriptCreate.$OOPactionscriptRetrieve.$OOPactionscriptUpdate.$OOPactionscriptDelete; 

//	uitvoer php script !!!! ==============================================================
$uitvoer="<h1>table values</h1>";
$Qt="SELECT * FROM `".$tabelnaam."`;";
$uitvoer.=$Qt;


?>