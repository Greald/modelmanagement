<?php
// ori: https://ghvernuft.nl/os/formgenerator.php?tabel=projectElements&velden=fieldname,fieldtype,fieldlength,fieldvalue&types=VARCHAR,VARCHAR,INT,VARCHAR&lengten=16,16,16,16&schermnamen=veldnaam,veld+type,veld+lengte,waarde&labelcols=col-sm-5+col-md-4+col-lg-3,col-sm-5+col-md-4+col-lg-3,col-sm-5+col-md-4+col-lg-3,col-sm-5+col-md-4+col-lg-3&veldcols=col-sm-7+col-md-8+col-lg-9,col-sm-7+col-md-8+col-lg-9,col-sm-7+col-md-8+col-lg-9,col-sm-7+col-md-8+col-lg-9&schermtypen=text,select,number,text&opties=[],[VARCHAR,TINYINT,TEXT,SMALLINT,MEDIUMINT,INT,BIGINT,FLOAT,DOUBLE,DECIMAL,DATETIME,TIMESTAMP,TIME,YEAR,CHAR,TINYBLOB,TINYTEXT,BLOB,MEDIUMBLOB,MEDIUMTEXT,LONGBLOB,LONGTEXT,ENUM,SET],[],[]&vereist=required,required,required,free

require_once "modules/syst/ProjectIni.class.php"; // incl "dbconnect.php"
$DB = null; // close database connection $DB

class ProjectModel extends ProjectIni
{
	//properties

	 // 'dataMember ElementElementTabelName'
	protected $mElementTabelName = "";
	// ElementTabelName-property accessor / mutator alias 'getter and setter'
	public function getElementTabelName(){return $this->mElementTabelName;}
	public function setElementTabelName($value)
	 {
	 // Business rule
	 if (true) // your business rule's condition here
	 $this->mElementTabelName = $value;
	 else
	 throw new clsExceptionInvalidElementTabelName( $value." invalid ElementTabelName" );
	 }

	 // 'dataMember Formid'
	protected $mFormid = "";
	// Fieldname-property accessor / mutator alias 'getter and setter'
	public function getFormid(){return $this->mFormid;}
	public function setFormid($value)
	 {
	 // Business rule
	 if (true) // your business rule's condition here
	 $this->mFormid = $value;
	 else
	 throw new clsExceptionInvalidFormid( $value." invalid Formid" );
	 }

	 // 'dataMember Fieldname'
	protected $mFieldname = "";
	// Fieldname-property accessor / mutator alias 'getter and setter'
	public function getFieldname(){return $this->mFieldname;}
	public function setFieldname($value)
	 {
	 // Business rule
	 if (true) // your business rule's condition here
	 $this->mFieldname = $value;
	 else
	 throw new clsExceptionInvalidFieldname( $value." invalid Fieldname" );
	 }

	 // 'dataMember Fieldtype'
	protected $mFieldtype = "";
	// Fieldtype-property accessor / mutator alias 'getter and setter'
	public function getFieldtype(){return $this->mFieldtype;}
	public function setFieldtype($value)
	 {
	 // Business rule
	 if (true) // your business rule's condition here
	 $this->mFieldtype = $value;
	 else
	 throw new clsExceptionInvalidFieldtype( $value." invalid Fieldtype" );
	 }

	 // 'dataMember Fieldlength'
	protected $mFieldlength = "";
	// Fieldlength-property accessor / mutator alias 'getter and setter'
	public function getFieldlength(){return $this->mFieldlength;}
	public function setFieldlength($value)
	 {
	 // Business rule
	 if (true) // your business rule's condition here
	 $this->mFieldlength = $value;
	 else
	 throw new clsExceptionInvalidFieldlength( $value." invalid Fieldlength" );
	 }


	// Constructor
	//public function __construct($ElementTabelName,$Formid,$Fieldname,$Fieldtype,$Fieldlength)
	public function __construct($ElementTabelName)//,$Fieldname)
	{
		try
		{
			$this->setElementTabelName($ElementTabelName);
			//$this->setFieldname($Fieldname);

			// pick the latest entered matching record
			$hiernu = $this->Retrieve(" WHERE `tablename` = '".$ElementTabelName."' ".
//			" AND `fieldname` ='".$Fieldname."' ".
			" ORDER BY `formid` DESC ");
//			if($hiernu['QRESULT'] == [])

			//echo "\n<br/>".self::class." ".__FUNCTION__." ".__LINE__."\n"; print_r($hiernu);
//			if($hiernu == ['log'=>$hiernu['log']]) // alleen $hiernu['log'] bestaat in $hiernu
			if($this->RetrieveResult($hiernu) == [])
			{
				$this->setFieldname(NULL);
				$this->setFormid(NULL);
				$this->setFieldtype(NULL);
				$this->setFieldlength(NULL);			
			}
			else
			{
				$this->setFieldname($hiernu[0]['fieldname']);
				$this->setFormid($hiernu[0]['formid']);
				$this->setFieldtype($hiernu[0]['fieldtype']);
				$this->setFieldlength($hiernu[0]['fieldlength']);			
			}
		}
		catch (\PDOException $ex) 
		{ 
		return "Er is een databasefout: " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
		return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
		$DB = null; 
		} 
	}


	//methods

	public function Create($Formid,$Fieldname,$Fieldtype,$Fieldlength)
	 { 
		 $DB = null; // only intentionally establish database connection here!
		 $DB = new dbConnect();
			 
		 try 
		 { 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/ 
			$uit = self::class."->".__FUNCTION__."(..):\n<br/>";
			$XCreate = "CREATE TABLE IF NOT EXISTS `". parent::PROJECTMODEL_PREFIX . parent::PROJECTMODEL."` 
				(
				`".parent::PROJECTMODEL."Rec` INT(8) AUTO_INCREMENT PRIMARY KEY,
				`formid` INT(10),
				`tablename` VARCHAR(32), 
				`fieldname` VARCHAR(32), 
				`fieldtype` VARCHAR(16), 
				`fieldlength` INT(4) 
				);";
			$DB->exec($XCreate);			
			$uit .= $XCreate;
			$uit .= "\n<br/>";

			if(null !== $Formid && null !== $Fieldname && null !== $Fieldtype && null !== $Fieldlength && TRUE)
			{
				$uit .= $this->Delete(" WHERE `formid` ='".$Formid."' AND `fieldname` = '".$Fieldname."' ");
				$uit .= "\n<br/>";
				
				$XCreate = "INSERT 
				INTO `".parent::PROJECTMODEL_PREFIX.parent::PROJECTMODEL."` 
				(
					`formid`, 
					`tablename`,
					`fieldname` , 
					`fieldtype` , 
					`fieldlength`
				) 
				VALUES 
				(
					'".$Formid."', 
					'".parent::PROJECTMODEL_PREFIX.$this->getElementTabelName()."', 
					'".$Fieldname."', 
					'".$Fieldtype."', 
					'".$Fieldlength."'
				);";
				$DB->exec($XCreate);
				$uit .= $XCreate;
				$uit .= "\n<br/>";
			}
			return $uit;
		 } 
		 catch (\PDOException $ex) 
		 { 
		 return "Er is een databasefout: " . $ex->getMessage(); 
		 } 
		 
		 catch (\Exception $ex) 
		 { 
		 return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		 } 
		 
		 finally //close database connection 
		 { 
		 $DB = null; 
		 } 
	 } 


	public function Retrieve($WHEREcondition) // array(resultaten, log-veld)
	{ 
		$DB = null; // only intentionally establish database connection here!	 
		$DB = new dbConnect();

		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/ 
			$src = self::class."->".__FUNCTION__."(..)";
			
			$QRetrieve = "SELECT * FROM `".parent::PROJECTMODEL_PREFIX.parent::PROJECTMODEL."` ".$WHEREcondition.";";		 
			//echo "=\n<br/>".$QRetrieve."=";

			$Retrieve = $DB->Q2Assoc($QRetrieve);
			//echo "\n<br/>".self::class." ".__FUNCTION__." ".__LINE__."\n"; print_r($Retrieve);
			$Retrieve['log'] = $QRetrieve;
			$Retrieve['src'] = $src;
			return $Retrieve;
		} 

		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout: " . $ex->getMessage(); 
		} 
		
		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 


	public function RetrieveResult($RetrievedResult) // $this->Retrieve($WHEREcondition) >> array(resultaten)
	{
		unset($RetrievedResult['src']);
		unset($RetrievedResult['log']);
		return $RetrievedResult;
	}

	public function RetrieveSource($RetrievedResult) // $this->Retrieve($WHEREcondition) >> SQL statement
	{
		return $RetrievedResult['src'];
	}
	
	public function RetrieveSQL($RetrievedResult) // $this->Retrieve($WHEREcondition) >> SQL statement
	{
		return $RetrievedResult['log'];
	}
	
		
	public function Update($parameterArr,$waardenArr,$WHEREcondition)
	{ 
		$DB = null; // only intentionally establish database connection here!
		$DB = new dbConnect();

		try 
		{ 
		 /* 
		 your code here 
		 using: $this->getProperty(), 
		 NOT: $this->mProperty 
		 for reasons of Encapsulation! 
		 */
		 $uit = self::class."->".__FUNCTION__."(..):\n<br/>"; 
		 $XUpdate  = "UPDATE `".parent::PROJECTMODEL_PREFIX.parent::PROJECTMODEL."` SET ";
		//			 $XUpdate += "`ProjectmodelRec`=[value-1],`formid`=[value-2],`tabelnaam`=[value-3],`veldnaam`=[value-4],`beschrijving`=[value-5]".
		 $PP = sizeof($parameterArr); 
		 if($PP == sizeof($waardenArr))
		 {
		 	for($P=0; $P<$PP; $P++)
		 	{
				$XUpdate .= "`" . $parameterArr[$P] . "` = '" . $waardenArr[$P] . "',";
			}
			$XUpdate = substr($XUpdate,0,-1); // remove latest comma
		 }
		 else
		 {
		 	throw new Exception("Mismatch between parameter array and values array");
		 }
		 $XUpdate .= " ".$WHEREcondition.";\n";
		 $DB->exec($XUpdate);
		 $uit .= "";
		 return "\n<br/>".$XUpdate;
		} 
		catch (\PDOException $ex) 
		{ 
		return "Er is een databasefout: " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
		return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
		$DB = null; 
		} 
	} 


	public function Delete($WHEREcondition)
	{ 
		$DB = null; // only intentionally establish database connection here!
		$DB = new dbConnect();
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			$uit = self::class."->".__FUNCTION__."(..):\n<br/>";
			
			$XDelete = "DELETE FROM `".parent::PROJECTMODEL_PREFIX.parent::PROJECTMODEL."` ".$WHEREcondition.";\n";//WHERE `formid` ='".$this->getFormid()."'";
			//echo $XDelete;
			$DB->exec($XDelete);

			$uit.= $XDelete;
			$uit.="\n<br/>";
			return $uit;
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout: " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 

	public function dispose(){/* your dispose function code here */}

	public function show()
	{
		return print_r([parent::PROJECTMODEL_PREFIX,self::PROJECTMODEL,$this], TRUE);
	}
}

if(!class_exists('clsExceptionInvalidElementTabelName'))
	{class clsExceptionInvalidElementTabelName extends \Exception {};}
if(!class_exists('clsExceptionInvalidFormid'))
	{class clsExceptionInvalidFormid extends \Exception {};}
if(!class_exists('clsExceptionInvalidFieldname'))
	{class clsExceptionInvalidFieldname extends \Exception {};}
if(!class_exists('clsExceptionInvalidFieldtype'))
	{class clsExceptionInvalidFieldtype extends \Exception {};}
if(!class_exists('clsExceptionInvalidFieldlength'))
	{class clsExceptionInvalidFieldlength extends \Exception {};}
?>