<?php

?>
<form name="formvelden" class="form-horizontal" role="form">
	<h2>database table = oop class</h2>

<div class="form-group row">
<div class="col-lg-6">
<input class="form-control btn btn-primary" type="button" value="reset" onclick="document.forms['formvelden'].reset();document.forms['formgenerator'].reset(); controls = '';"/>
</div><!-- class="col-lg-6" -->
</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="tabelnaam">table name
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="tabelnaam" class="form-control" required="required" placeholder="tabel"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<h3>add fields</h3>
	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="veldnaam">field name
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="veldnaam" class="form-control" required="required" placeholder="field name"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="veldtype">data type
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<select name="veldtype" class="form-control" required="required">
				<option>-- kies --</option> 
				<option>VARCHAR</option> 
				<option>TINYINT</option>
				<option>TEXT</option><option>DATE</option>
				<option>SMALLINT</option>
				<option>MEDIUMINT</option>
				<option>INT</option>
				<option>BIGINT</option>
				<option>FLOAT</option>
				<option>DOUBLE</option>
				<option>DECIMAL</option>
				<option>DATETIME</option>
				<option>TIMESTAMP</option>
				<option>TIME</option>
				<option>YEAR</option>
				<option>CHAR</option>
				<option>TINYBLOB</option>
				<option>TINYTEXT</option>
				<option>BLOB</option>
				<option>MEDIUMBLOB</option>
				<option>MEDIUMTEXT</option>
				<option>LONGBLOB</option>
				<option>LONGTEXT</option>
				<option>ENUM</option>
				<option>SET</option>
			</select>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="veldlengte">field length
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="number" name="veldlengte" class="form-control" required="required" value="16"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<h2>input screen</h2>
	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="schermnaam">screen field label = name
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="schermnaam" class="form-control" required="required" placeholder="field label"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="labelwidth">label cols
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="labelwidth" class="form-control" required="required" value="col-sm-5 col-md-4 col-lg-3"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="fieldwidth">screen field cols
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="fieldwidth" class="form-control" required="required" value="col-sm-7 col-md-8 col-lg-9"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="schermtype">screen field type
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<select name="schermtype" class="form-control" required="required" onchange="document.forms['formvelden'].elements['opties'].disabled='disabled';var tset=this.options[this.selectedIndex].value;if(tset=='radio' || tset=='checkbox' || tset=='select'){document.forms['formvelden'].elements['opties'].disabled=false;}">
				<option>-- kies --</option> 
				<option value="text">text</option> 
				<option value="textarea">textarea</option> 
				<option value="button">button</option> 
				<option value="radio">radio</option> 
				<option value="checkbox">checkbox</option> 
				<option value="select">select</option>
				<option value="submit">submit</option>

				<option value="number">number</option>
				<option value="password">password</option>
			    <option value="range">(slider) range</option>

			    <option value="url">url</option>
			    <option value="email">email</option>
			    <option value="color">color</option>
			    <option value="tel">tel (only in Safari 8)</option>
			    <option value="search"><s>search</s></option>
			    <option value="date">date</option>
			    <option value="datetime">datetime</option>
			    <option value="datetime-local">datetime-local</option>
			    <option value="month">month</option>
			    <option value="time">time</option>
			    <option value="week">week</option>
			</select>
		</div><!-- class="col-lg-6" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="opties">options <i>ordered</i>,<br/>comma separated
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="text" name="opties" class="form-control" disabled="disabled" value=""/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="require">required field
		</label>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<input type="radio" name="require" value="required" checked="checked"/>
			required&nbsp;|&nbsp;free
			<input type="radio" name="require" value="free"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->

	<div class="form-group row">
		<label class="control-label col-lg-3 col-md-4 col-sm-5" for="afronden">
		</label>
		<div class="col-lg-4 col-md-4 col-sm-3">
			<input type="button" class="form-control btn btn-primary" value="add field"
				onclick="document.forms['formgenerator'].elements['tabel'].value = document.forms['formvelden'].elements['tabelnaam'].value;
						 document.forms['formgenerator'].elements['velden'].value += ','+ document.forms['formvelden'].elements['veldnaam'].value;

						 document.forms['formgenerator'].elements['types'].value += ','+ document.forms['formvelden'].elements['veldtype'].options[document.forms['formvelden'].elements['veldtype'].selectedIndex].text;

						 document.forms['formgenerator'].elements['lengten'].value += ','+ document.forms['formvelden'].elements['veldlengte'].value;
						 document.forms['formgenerator'].elements['schermnamen'].value += ','+ document.forms['formvelden'].elements['schermnaam'].value;

						 document.forms['formgenerator'].elements['labelcols'].value += ','+ document.forms['formvelden'].elements['labelwidth'].value;
						 document.forms['formgenerator'].elements['veldcols'].value += ','+ document.forms['formvelden'].elements['fieldwidth'].value;

						 document.forms['formgenerator'].elements['schermtypen'].value += ','+ document.forms['formvelden'].elements['schermtype'].options[document.forms['formvelden'].elements['schermtype'].selectedIndex].text;
						 document.forms['formgenerator'].elements['opties'].value += ',['+ document.forms['formvelden'].elements['opties'].value +']';
						 document.forms['formgenerator'].elements['vereist'].value += ','+ document.forms['formvelden'].elements['require'].value;
						 document.forms['formvelden'].elements['opties'].value='';
						 document.forms['formvelden'].elements['opties'].disabled='disabled';
			"/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
		
		<div class="col-lg-1 col-md-1 col-sm-1"><br/></div>

		<div class="col-lg-4 col-md-3 col-sm-3">
			<input type="button" class="form-control btn btn-info" value="finish"
				onclick="kapaf(document.forms['formgenerator'].elements['velden']);
						 kapaf(document.forms['formgenerator'].elements['types']);
						 kapaf(document.forms['formgenerator'].elements['lengten']);
						 kapaf(document.forms['formgenerator'].elements['schermnamen']);
						 kapaf(document.forms['formgenerator'].elements['labelcols']);
						 kapaf(document.forms['formgenerator'].elements['veldcols']);
						 kapaf(document.forms['formgenerator'].elements['schermtypen']);
						 kapaf(document.forms['formgenerator'].elements['opties']);
						 kapaf(document.forms['formgenerator'].elements['vereist']);
						 document.forms['formgenerator'].submit();
						 "/>
		</div><!-- class="col-lg-9 col-md-8 col-sm-7" -->
	</div><!-- class="form-group row" -->
</form>
<script>function kapaf(str) {str.value=str.value.substr(1);}</script>
</div>


<form name="formgenerator" action="<?php echo $_SERVER['PHP_SELF']; ?>" style="visibility:hidden">
<table>
<tr><td>
<br><script>typesscheidingsteken='';schermtypesscheidingsteken='';</script>
tabelnaam</td><td><input type="text" name="tabel"></td><td></td></tr>
<tr><td>
veldnamen: [</td><td><input type="text" name="velden">]</td><td>(<i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
veldtypen: [</td><td><input type="text" name="types">]</td><td>(<select onchange="document.forms['formgenerator'].elements['types'].value+=typesscheidingsteken+this.options[this.selectedIndex].text;this.selectedIndex=0;typesscheidingsteken=',';"><option>-- kies --</option> <option>VARCHAR</option> <option>TINYINT</option><option>TEXT</option><option>DATE</option><option>SMALLINT</option><option>MEDIUMINT</option><option>INT</option><option>BIGINT</option><option>FLOAT</option><option>DOUBLE</option><option>DECIMAL</option><option>DATETIME</option><option>TIMESTAMP</option><option>TIME</option><option>YEAR</option><option>CHAR</option><option>TINYBLOB</option><option>TINYTEXT</option><option>BLOB</option><option>MEDIUMBLOB</option><option>MEDIUMTEXT</option><option>LONGBLOB</option><option>LONGTEXT</option><option>ENUM</option><option>SET</option>    </select> <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
veldlengten: [</td><td><input type="text" name="lengten">]</td><td>(<i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
schermnamen: [</td><td><input type="text" name="schermnamen">]</td><td>(input's namen; <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
labelcols: [</td><td><input type="text" name="labelcols">]</td><td>(input label cols; <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
veldcols: [</td><td><input type="text" name="veldcols">]</td><td>(input veld cols; <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
schermtypen: [</td><td><input type="text" name="schermtypen">]</td><td>(<select onchange="document.forms['formgenerator'].elements['schermtypen'].value+=schermtypesscheidingsteken+this.options[this.selectedIndex].text;this.selectedIndex=0;schermtypesscheidingsteken=',';"><option>-- kies --</option> <option>text</option> <option>textarea</option> <option>password</option> <option>button</option> <option>radio</option> <option>checkbox</option> <option>select</option> <option>submit</option> </select> <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
opties: [</td><td><input type="text" name="opties">]</td><td>(alleen voor schermtypen select, radio, checkbox; <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td>
vereist: [</td><td><input type="text" name="vereist">]</td><td>vereist j/n <i>op volgorde</i>, door komma's gescheiden)</td></tr>
<tr><td></td><td>
<input type="submit" value="vort"></td><td></td></tr>
</table>
</form>
