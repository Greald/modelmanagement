<?php
class View
{

	//properties

	// 'dataMember HtmlBlock'
	protected $mHtmlBlock = "";
	// HtmlBlock-property accessor / mutator alias 'getter and setter'
	public function getHtmlBlock(){return $this->mHtmlBlock;}
	public function setHtmlBlock($value)
	{
		// Business rule
		if (true) // your business rule's condition here
			$this->mHtmlBlock = $value;
		else
			throw new clsExceptionInvalidHtmlBlock( $value." invalid HtmlBlock" );
	}


// Constructor
//public function __construct($value){$this->setHtmlBlock($value);}

// ProjectController

	//properties

	// 'dataMember PhpScript'
	protected $mPhpScript = "";
	// PhpScript-property accessor / mutator alias 'getter and setter'
	public function getPhpScript(){return $this->mPhpScript;}
	//Collection
	public function addPhpScriptElement($PhpScriptElement)
	{
		// Business rule
		if (is_array($this->mPhpScript))
		{
		}
		else
		{
			$this->mPhpScript = [];
		}
		$this->mPhpScript[] = $PhpScriptElement;
	}

	public function setPhpScript($value){return $this->addPhpScriptElement($value);}

	public function removePhpScriptElement($PhpScriptElement)
	{
		foreach($this->mPhpScript as $list => $pal)
		{
			if($pal == $PhpScriptElement)
			{
				unset($this->mPhpScript[$list]);
			}
		}
	}

	function InputPhpScriptCollection($listArr)
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			if(!is_array($listArr)){$listArr = [$listArr];}
			foreach($listArr as $elm)
			{
				$this->addPhpScriptElement($elm);
			}
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 



	//methods
	
	public function PageOut($phpSnippetsArr)// array van php-scripts op volgorde van uitvoering
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			$this->InputPhpScriptCollection($phpSnippetsArr);
			$page="";
			foreach($this->getPhpScript() as $phpSnippet)
			{
				$page .= $phpSnippet;
			} 
			
			// de externe teijdelijke file-hack
			$tmpfname = @tempnam("./tmp", "hulpfile");
			$handle = fopen($tmpfname, "w");
			fwrite($handle, $page);
			fclose($handle);

			// !!!!!!!!!!!!!!!!!!!!!!!!! UITVOEREN !!!!!!!!!!!!!!
			// parse en stuur de php-code daarmee naar de client: 
			require $tmpfname; 
			// na gebruik weer verwijderen:
			unlink($tmpfname); 
			return ["log" => $tmpfname ." verwijderd. "];
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 

// einde ProjectController

	//methods

	public function ParsedPhpScript($phpScriptFile)// NIET TE GEBRUIKEN
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/

			ob_start();
			require $phpScriptFile;	
			$domdoc = (new DOMElement("naamomdatdatmoet",ob_get_flush()))->nodeValue; // http://php.net/manual/en/class.domelement.php#86596
			die("is ie al afgegaan?");
			ob_end_clean();
			$this->setHtmlBlock($domdoc);
			return $this->getHtmlBlock();
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout: " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout: " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 

	public function dispose(){/* your dispose function code here */}

}

if(!class_exists('clsExceptionInvalidHtmlBlock'))
	{class clsExceptionInvalidHtmlBlock extends \Exception {};}
if(!class_exists('clsExceptionInvalidPhpScript'))
	{class clsExceptionInvalidPhpScript extends \Exception {};}

?>