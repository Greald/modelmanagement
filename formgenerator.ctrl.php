<?php
$parkeys = 'tabel,velden,types,lengten,schermnamen,schermtypen'; // vereiste parameters voor uitvoer
$inkeys = TRUE;
foreach(explode(",",$parkeys) as $leutel)
{
	$inkeys = $inkeys && isset($_GET[$leutel]);
}
if($_GET == [] || !$inkeys){$_GET = [];}
//echo "na: "; print_r($_GET);	

require_once "htmlBlokken.view.php"; // incl class View
require_once "ProjectController.class.php";
$TestController = new ProjectController();

if($_GET !== [])
{
	$engine = "generatorBlokkenActionScript.php";
	$title = "generatorblokken";
	$incontainer = "generatorBlokkenAction.view.php";
}
else
{
	$engine = "";
	$title = "formgenerator";
	$incontainer = "formgeneratorform.view.php";
}

$phpSnippetsArr=[];
if(strlen($engine)>0){$phpSnippetsArr[]= file_get_contents($engine);}
$phpSnippetsArr[]=(new View($pageHeadBS_RD))->getHtmlBlock();
$phpSnippetsArr[]=(new View("\n\n\t\t<title>".$title."</title>\n\t\t"))->getHtmlBlock();
$phpSnippetsArr[]=(new View($pageStartBodyBS_RD))->getHtmlBlock();
$phpSnippetsArr[]=file_get_contents($incontainer);
$phpSnippetsArr[]=(new View($pageEndPageBS_RD))->getHtmlBlock();
	
$tempfile = $TestController->PageOut($phpSnippetsArr);
//echo "\n<br/>\n<br/><!-- ". print_r($tempfile, true)." -->";

?>