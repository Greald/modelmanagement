<?php
/*


// Unicode-proof htmlentities. 
Bron: http://php.net/manual/en/function.htmlentities.php#107985
// Returns 'normal' chars as chars and weirdos as numeric html entites.
function superentities( $str ){
    // get rid of existing entities else double-escape
    $str = html_entity_decode(stripslashes($str),ENT_QUOTES,'UTF-8');
    $ar = preg_split('/(?<!^)(?!$)/u', $str );  // return array of every multi-byte character
    foreach ($ar as $c){
        $o = ord($c);
        if ( (strlen($c) > 1) || //* multi-byte [unicode] * /
            ($o <32 || $o > 126) || //* <- control / latin weirdos -> * /
            ($o >33 && $o < 40) ||//* quotes + ambersand * /
            ($o >59 && $o < 63) //* html * /
        ) {
            // convert to numeric entity
            $c = mb_encode_numericentity($c,array (0x0, 0xffff, 0, 0xffff), 'UTF-8');
        }
        $str2 .= $c;
    }
    return $str2;
}

//============================
* @aanroep: mbHTMLnumenti($str)
* 
* @param string $str in UTF-8
* 
* @return string $str 
* waarin alle andere tekens dan basisletters, cijfers, whitespaces en @ , . ; : 
* vervangen zijn door hun multibyte HTML numerieke entiteiten in de vorm &#xxxxx;
*/
function mbStringToArray ($string) // source: http://php.net/manual/en/function.mb-split.php#80046
	{
//echo "\n<br/>".$string;	
    $strlen = mb_strlen($string);
    $array=[];
    while ($strlen) 
	    {
	    $array[] = mb_substr($string,0,1,"UTF-8");
	    $string = mb_substr($string,1,$strlen,"UTF-8");
	    $strlen = mb_strlen($string);
	    }
//print_r($array);
    return $array;
	}
function mbHTMLnumenti($str) // geldig voor UTF-8 en met regular expression [^\w\s@.,;:]
	{
	$str =  mbStringToArray ($str);

	$pattern =  "[^\w\s@.,;:]";
	$strLength = sizeof($str); 
	for($L=0;$L < $strLength; $L++)
		{
		if(mb_ereg_match ($pattern , $str[$L]))
			{
//			echo "\n".$str[$L];
			$str[$L] = mb_encode_numericentity ($str[$L], [0x0, 0xffffff, 0, 0xffffff], 'UTF-8');
//			echo "\n".$str[$L];
			}
		else
			{
//			echo "\n".$str[$L];
			}
		}
	return implode("",$str);
	}

$malicious = "";
function stripReeks($arr)
         {
         global $malicious;
         
         while(list($key,$str)=each($arr))
             {
             if(is_array($str))
             	{
             	$str=stripReeks($str); // RECURSIEVE AANROEP              	
             	}
             elseif(is_string($str))
             	{
             	$arr[$key] = mbHTMLnumenti($str);
            	}
             }
         return $arr;
         }


if(isset($_POST))
{
	$post		=	$_POST;
	$_POST		=	stripReeks($_POST);
}
if(isset($_GET))
{
	$getQS		= 	$_GET;
	$_GET		=	stripReeks($_GET);
}
if(isset($_COOKIE))
{
	$_COOKIE	=   stripReeks($_COOKIE);
}
	

//$teststring = "b0ven op 4et ®od€ k1€€dje, bo￦en op de ߷li#ant!";
//print_r(mbStringToArray($teststring));
//echo "<br>\n".mbHTMLnumenti($teststring)."<br>\n";

//$testArray = explode(" ",$teststring);
//print_r($testArray);
//print_r(stripReeks($testArray));


function zuiver_querystring($QryStr)
{
//echo "\n<br/>". $QryStr;
parse_str ($QryStr, $QryArr);
//echo "\n<br/>";print_r($QryArr);
$QSuit="";
foreach($QryArr as $key=>$value)
{
	$QSuit .= rawurlencode(rawurldecode($key))."=".rawurlencode(rawurldecode($value))."&";
}
$QSuit = substr($QSuit,0,-1);
//echo "\n<br/>". $QSuit;
return $QSuit;
}

function vraagteken_zuiver_querystring($QryStr)
{
$QSuit = zuiver_querystring($QryStr);
if (strlen($QSuit)>0){$QSuit = "?".$QSuit;}
return $QSuit;
}

// decode mbHTMLnumenti: http://php.net/manual/en/function.html-entity-decode.php#104617
//echo "een teken van leven";print_r($getQS);
?>