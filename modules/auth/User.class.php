<?php
//require_once "../../ProjectIni.class.php";
require_once "../../ProjectModel.class.php";
$TestModel = new ProjectModel(time(),"Projectnaam","Users","Gebruikersgegevens en toegangscodes");
echo "\n<br/>Created Table : ".$TestModel->Create()."\n<br/>";

require_once "../../ProjectElements.class.php":
$userElements = new ProjectElements("Users",$Fieldname);



class User
{

	//properties

	// 'dataMember UserID'
	protected $mUserID = NULL;
	// UserID-property accessor / mutator alias 'getter and setter'
	public function getUserID(){return $this->mUserID;}
	public function setUserID($value)
	{
		// Business rule
		if (true) // your business rule's condition here
			$this->mUserID = $value;
		else
			throw new clsExceptionInvalidUserID( $value." invalid UserID" );
	}

	// 'dataMember UserName'
	protected $mUserName = NULL;
	// UserName-property accessor / mutator alias 'getter and setter'
	public function getUserName(){return $this->mUserName;}
	public function setUserName($value)
	{
		// Business rule
		if (true) // your business rule's condition here
			$this->mUserName = $value;
		else
			throw new clsExceptionInvalidUserName( $value." invalid UserName" );
	}

	// 'dataMember PassWord'
	protected $mPassWord = NULL;
	// PassWord-property accessor / mutator alias 'getter and setter'
	public function getPassWord(){return $this->mPassWord;}
	public function setPassWord($value)
	{
		// Business rule
		if (true) // your business rule's condition here
			$this->mPassWord = $value;
		else
			throw new clsExceptionInvalidPassWord( $value." invalid PassWord" );
	}

	// 'dataMember PassWord'
	protected $mLastRequest = NULL;
	// PassWord-property accessor / mutator alias 'getter and setter'
	public function getLastRequest(){return $this->mLastRequest;}
	public function setLastRequest($value)
	{
		// Business rule
		/*
		tijdverloop tussen time() en getLastrequest() moet groter zijn dan -zeg- 2s om wachtwoordhackers te frustreren.
		Indien 
		time() - getLastrequest() < 2s 
		vernietig inlog-request en geef inlogpagina terug
		*/
		if (true) // your business rule's condition here
			$this->mLastRequest = $value;
		else
			throw new clsExceptionInvalidLastRequest( $value." invalid LastRequest" );
	}

	// 'dataMember Account'
	protected $mAccount = NULL;
	// Account-property accessor / mutator alias 'getter and setter'
	public function getAccount(){return $this->mAccount;}
	public function setAccount($value)
	{
		// Business rule
		if (true) // your business rule's condition here
			$this->mAccount = $value;
		else
			throw new clsExceptionInvalidAccount( $value." invalid Account" );
	}


	// Constructor
	public function __construct($UserID,$UserName,$PassWord,$LastRequest,$Account)
	{
		$this->setUserID($UserID);
		$this->setUserName($UserName);
		$this->setPassWord($PassWord);
		$this->setPassWord($LastRequest);
		$this->setAccount($Account);
	}


	//methods

	public function Create($formid)
	{ 
		$DB = null; // only intentionally establish database connection here!
		$DB = new dbConnect();

		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/

			//		 if(isset($_POST['formid']) && isset($_POST['userID']) && isset($_POST['userName']) && isset($_POST['passWord']) && isset($_POST['account']) && TRUE)
			if(
				   $this->getUserID() !== NULL 
				&& $this->getUserName() !== NULL 
				&& $this->getPassWord() !== NULL 
				&& $this->getAccount() !== NULL 
				&& $this->getLastRequest() !== NULL
			)
			{
			 	$CT = "CREATE TABLE IF NOT EXISTS `".($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX."user` 
				 	(
				 	`userRec` INT(7) AUTO_INCREMENT PRIMARY KEY,
				 	`formid` INT(10),
				 	`userID` VARCHAR(64), 
				 	`userName` VARCHAR(32), 
				 	`passWord` VARCHAR(64), 
				 	`LastRequest` INT(10), 
				 	`account` VARCHAR(8)
				 	);";
			 	$DB->exec($CT);
			 	
			 	$DB->exec($this->Delete(" WHERE `formid` ='".$formid."' "););

				$DB->exec("
					INSERT INTO 
					`".($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX."user` (
						`formid`, 
						`userID` , 
						`userName` , 
						`passWord` , 
						`LastRequest`, 
						`account` 
						) 
					VALUES ('".
						$formid."', '".
						$this->getUserID()."' , '".
						$this->getUserName()."' , '".
						$this->getPassWord()."' , '".
						$nu."', '".
						$this->getAccount().
						"' );
						");		 	
		 	} 
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 


	public function Retrieve($formid)
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/ 

			/*
			tijdverloop tussen time() en getLastrequest() moet groter zijn dan -zeg- 2s om wachtwoordhackers te frustreren.
			Indien 
			time() - getLastrequest() < 2s 
			vernietig inlog-request en geef inlogpagina terug
			*/
			$QslowRobot = "SELECT * FROM `".($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX."user` WHERE '".$this->getUserName()."'=`userName`";
		 	$nu = time();
		 	$teSnel = FALSE;
		 	$slowRobot = $DB->Q2Assoc($QslowRobot);
		 	if($slowRobot !== [])
		 	{					
			 	foreach($slowRobot as $userLine)
			 	{
					if($nu - $userLine['LastRequest'] < 2)
					{
						$teSnel = TRUE;
					}
				}
			 	if($teSnel)
				{		
					return "&#9940;"; // ⛔
				}		 	
				else
			 	{
					//$DB->exec("DELETE FROM `user` WHERE `formid` ='".$formid."'");
					$DB->exec($this->Delete(" WHERE `formid` ='".$formid."' "));
					$DB->exec($this->Update(["`LastRequest`"],[time()]," WHERE '".$this->getUserName()."' = `userName` AND '".$this->getPassWord()."' = `passWord` "));
				}
			}
			else
			{
				return; // geen match gevonden. Probeer opnieuw of ... Registratie
			}
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 


	public function Update($parameterArr,$waardenArr,$WHEREcondition)
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			$XUpdate = "UPDATE `".($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX."user` SET ";
			$PP = sizeof($parameterArr); 
			if($PP == sizeof($waardenArr))
			{
				for($P=0; $P<$PP; $P++)
				{
					$XUpdate .= "`" . $parameterArr[$P] . "` = '" . $waardenArr[$P] . "',";
				}
			$XUpdate = substr($XUpdate,0,-1); // remove latest comma
			$XUpdate.= $WHEREcondition;
			}
			else
			{
				throw new Exception("Mismatch between user parameter array and values array in  in ".__CLASS__." ".__METHOD__);
			}
			$XUpdate .= $WHEREcondition."; "; 
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 


	public function Delete($WHEREcondition)
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			return $DB->exec("DELETE FROM `".($ProjectIni = new ProjectIni)::PROJECTMODEL_PREFIX."user` ".$WHEREcondition); 
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 
		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 

	public function dispose(){/* your dispose function code here */}

}

if(!class_exists('clsExceptionInvalidUserID'))
	{class clsExceptionInvalidUserID extends \Exception {};}
if(!class_exists('clsExceptionInvalidUserName'))
	{class clsExceptionInvalidUserName extends \Exception {};}
if(!class_exists('clsExceptionInvalidPassWord'))
	{class clsExceptionInvalidPassWord extends \Exception {};}
if(!class_exists('clsExceptionInvalidLastRequest'))
	{class clsExceptionInvalidLastRequest extends \Exception {};}
if(!class_exists('clsExceptionInvalidAccount'))
	{class clsExceptionInvalidAccount extends \Exception {};}
?>