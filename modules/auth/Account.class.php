<?php
/*
class account
{
	//properties
	public $accountRec;
	public $accountNaam;
	public $accCode;
}

$QAccount = "CREATE TABLE IF NOT EXISTS `dbr_account` (
  `accountRec` int(7) AUTO_INCREMENT PRIMARY KEY,
  `accountNaam` varchar(64) DEFAULT NULL,
  `accCode` varchar(32) DEFAULT NULL
)";
*/

class Account
{

//properties

 // 'dataMember AccountID'
protected $mAccountID = "";
// AccountID-property accessor / mutator alias 'getter and setter'
public function getAccountID(){return $this->mAccountID;}
public function setAccountID($value)
 {
 // Business rule
 if (true) // your business rule's condition here
 $this->mAccountID = $value;
 else
 throw new clsExceptionInvalidAccountID( $value." invalid AccountID" );
 }

 // 'dataMember AccountNaam'
protected $mAccountNaam = "";
// AccountNaam-property accessor / mutator alias 'getter and setter'
public function getAccountNaam(){return $this->mAccountNaam;}
public function setAccountNaam($value)
 {
 // Business rule
 if (true) // your business rule's condition here
 $this->mAccountNaam = $value;
 else
 throw new clsExceptionInvalidAccountNaam( $value." invalid AccountNaam" );
 }

 // 'dataMember AccCode'
protected $mAccCode = "";
// AccCode-property accessor / mutator alias 'getter and setter'
public function getAccCode(){return $this->mAccCode;}
public function setAccCode($value)
 {
 // Business rule
 if (true) // your business rule's condition here
 $this->mAccCode = $value;
 else
 throw new clsExceptionInvalidAccCode( $value." invalid AccCode" );
 }


// Constructor
 public function __construct($AccountID,$AccountNaam,$AccCode)
 {
     $this->setAccountID($AccountID);
     $this->setAccountNaam($AccountNaam);
     $this->setAccCode($AccCode);
 }


//methods

public function Create( /* your arguments here */ )
 { 
	 $DB = null; // only intentionally establish database connection here!
	 try 
	 { 
		 /* 
		 your code here 
		 using: $this->getProperty(), 
		 NOT: $this->mProperty 
		 for reasons of Encapsulation! 
		 */ 
	 } 
	 catch (\PDOException $ex) 
	 { 
	 return "Er is een databasefout: " . $ex->getMessage(); 
	 } 
	 
	 catch (\Exception $ex) 
	 { 
	 return "Er is een fatale systeemfout: " . $ex->getMessage(); 
	 } 
	 
	 finally //close database connection 
	 { 
	 $DB = null; 
	 } 
 } 


public function Retrieve( /* your arguments here */ )
 { 
	 $DB = null; // only intentionally establish database connection here!
	 try 
	 { 
		 /* 
		 your code here 
		 using: $this->getProperty(), 
		 NOT: $this->mProperty 
		 for reasons of Encapsulation! 
		 */ 
	 } 
	 catch (\PDOException $ex) 
	 { 
	 return "Er is een databasefout: " . $ex->getMessage(); 
	 } 
	 
	 catch (\Exception $ex) 
	 { 
	 return "Er is een fatale systeemfout: " . $ex->getMessage(); 
	 } 
	 
	 finally //close database connection 
	 { 
	 $DB = null; 
	 } 
 } 


public function Update( /* your arguments here */ )
 { 
	 $DB = null; // only intentionally establish database connection here!
	 try 
	 { 
		 /* 
		 your code here 
		 using: $this->getProperty(), 
		 NOT: $this->mProperty 
		 for reasons of Encapsulation! 
		 */ 
	 } 
	 catch (\PDOException $ex) 
	 { 
	 return "Er is een databasefout: " . $ex->getMessage(); 
	 } 
	 
	 catch (\Exception $ex) 
	 { 
	 return "Er is een fatale systeemfout: " . $ex->getMessage(); 
	 } 
	 
	 finally //close database connection 
	 { 
	 $DB = null; 
	 } 
 } 


public function Delete( /* your arguments here */ )
 { 
	 $DB = null; // only intentionally establish database connection here!
	 try 
	 { 
		 /* 
		 your code here 
		 using: $this->getProperty(), 
		 NOT: $this->mProperty 
		 for reasons of Encapsulation! 
		 */ 
	 } 
	 catch (\PDOException $ex) 
	 { 
	 return "Er is een databasefout: " . $ex->getMessage(); 
	 } 
	 
	 catch (\Exception $ex) 
	 { 
	 return "Er is een fatale systeemfout: " . $ex->getMessage(); 
	 } 
	 
	 finally //close database connection 
	 { 
	 $DB = null; 
	 } 
 } 

public function dispose(){/* your dispose function code here */}

}

if(!class_exists('clsExceptionInvalidAccountID'))
	{class clsExceptionInvalidAccountID extends \Exception {};}
if(!class_exists('clsExceptionInvalidAccountNaam'))
	{class clsExceptionInvalidAccountNaam extends \Exception {};}
if(!class_exists('clsExceptionInvalidAccCode'))
	{class clsExceptionInvalidAccCode extends \Exception {};}
?>