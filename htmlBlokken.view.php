<?php
require_once "View.class.php";

// HeadBS_RD
$pageHeadBS_RD = <<<BLOK
<!DOCTYPE html>
<html lang="nl">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
BLOK;

//StartBodyBS_RD
$pageStartBodyBS_RD = <<<BLOK

	</head>
	<body>
		<div class="container-fluid">
BLOK;

//EndPageBS_RD
$pageEndPageBS_RD = <<<BLOK

		</div><!--container-fluid-->
	</body>
</html>
BLOK;



// TEST
$page= "";
$page .= (new View($pageHeadBS_RD))->getHtmlBlock();
$page .= (new View($pageStartBodyBS_RD))->getHtmlBlock();
$page .= (new View($pageEndPageBS_RD))->getHtmlBlock();
//die($page) ;
//
unset($page);
?>
