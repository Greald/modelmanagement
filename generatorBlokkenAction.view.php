<?php
echo "<a href=\"".$_SERVER['PHP_SELF']."\">anew</a>\n<br/>";

?>
<script src="./noop/HTMLfromCharCode.js"></script>
<textarea id="querystring" cols="48" rows="8"><?= rawurldecode($_SERVER['QUERY_STRING']) ;?></textarea>
<button onclick="location.replace('<?= $_SERVER['PHP_SELF'];?>?'+document.getElementById('querystring').value)">modify</button>

<h1>create table / class</h1>
<h2>query to MySQL database:</h2>
<?= $Qcreate; ?> 
<h2>php class generator</h2>
<a href="noop/classMaker.php?class=<?= $_GET['tabel'] ;?>&velden=<?= $_GET['velden'];?>" target="_blank">generate and edit class code</a>

<h1>generated form</h1>
<div id="embedform">
<?php
echo($display);
?>
</div>
<h2>Copy and embed form into your html script</h2>
<textarea cols="48" rows="12" id="selectie"></textarea>
<script>
document.getElementById('embedform').innerHTML = HTMLfromCharCode(document.getElementById('embedform').innerHTML);
document.getElementById('selectie').value = document.getElementById('embedform').innerHTML;
document.getElementById('embedform').innerHTML = "";
</script>

<textarea id="bootstrapped_form" cols="48" rows="12"></textarea>
<button onclick="document.getElementById('bootstrapped_toon').innerHTML = HTMLfromCharCode(document.getElementById('bootstrapped_form').value);">preview modified</button>
<h3>bootstrapped rd preview</h3>
<div id="bootstrapped_toon" style="background-color: #abcdef;border: 3px solid #aaccee;"></div>


<script>

	//verwerking querystring 'tabel=tabel&velden=value&types=--+kies+--&lengten=16&schermnamen=value&schermtypen=--+kies+--&opties=optiereeks'
	
	var tabel		= "<?= $_GET['tabel'];?>";
	var velden 		= <?= json_encode(explode(",",$_GET['velden'])); ?>;
	var types 		= <?= json_encode(explode(",",$_GET['types'])); ?>;
	var lengten 	= <?= json_encode(explode(",",$_GET['lengten'])); ?>;
	var schermnamen = <?= json_encode(explode(",",$_GET['schermnamen'])); ?>;
	var labelcols 	= <?= json_encode(explode(",",$_GET['labelcols'])); ?>;
	var veldcols 	= <?= json_encode(explode(",",$_GET['veldcols'])); ?>;
	var schermtypen = <?= json_encode(explode(",",$_GET['schermtypen'])); ?>;
	var opties 		= HTMLfromCharCode(<?= json_encode($_GET['opties']); ?>);
<?php
if(isset($_GET['opties']) && is_string($_GET['opties']))
{
?>
		//alert(opties);
		opties = opties.slice(1,-1); // -> opties like ["0,1,2"],["a,b,c"],["I,II,III"]
		opties = opties.split("],[");
		//alert(opties);
	function fopties(optiereeks) // optiereeks like ["0,1,2"],["a,b,c"],["I,II,III"]
	{
		var OO = optiereeks.length;
		for(var O=0; O < OO; O++)
		{
			optiereeks[O] = optiereeks[O].split(",");
		}
		return optiereeks;
	}
		opties = fopties(opties); // -> opties like ["0","1","2"],["a","b","c"],["I","II","III"]
<?php
}
else
{
?>
	var opties 		= [""];
<?php
}
?>
	var vereist 	= <?= json_encode(explode(",",$_GET['vereist'])); ?>;
	
	function fcontrols(schermnaam, labelcol, veldcol, vereist, schermtype, optiereeks)
	{
		var uit = "";
		uit += "\n<div class=\"row form-group\">";

// de labels
			uit += "\n<label class=\"control-label " + labelcol + "\" for=\"" + schermnaam + "\">";
			uit += schermnaam;
			uit += "\n</label>";
			
//de controls zelf
			uit += "\n<div class=\"" + veldcol + "\">";

		var veldnaam = " name=\"" + schermnaam + "\"";
		var formcontrol = " class=\"form-control\"";
		if(vereist == 'required')
		{
			vereist = ' required=\"required\"';
		}
		else
		{
			vereist = "";
		}		
		tussen = veldnaam + formcontrol + vereist;
			
		if(schermtype == "textarea")
		{
			uit += "\n<textarea "+ veldnaam + formcontrol + vereist +" cols=\"40\" rows=\"8\"></textarea>\n";
		}else
		if(schermtype == "select")
		{
			uit += "\n<select" + veldnaam + formcontrol + vereist + ">";
			var CC=optiereeks.length; for(var C=0; C < CC; C++)
			{
				uit += "\n<option value=\"" + optiereeks[C] + "\">" + optiereeks[C] + "</option>";
			}
			C=undefined; CC=undefined;			

			uit += "\n</select>";
			
		}else
		if(schermtype == "checkbox")
		{
			var CC=optiereeks.length; for(var C=0; C < CC; C++)
			{
				uit += "\n<div class=\"checkbox\"><label><input type=\"checkbox\" name=\"" + schermnaam + "\" value=\"" + optiereeks[C] + "\"/>" + optiereeks[C] + "</label></div><!--class=\"checkbox\"-->";
			}
			C=undefined; CC=undefined;	
			uit += "\n";		
		}else
		if(schermtype == "radio")
		{
			var CC=optiereeks.length; for(var C=0; C < CC; C++)
			{
				uit += "\n<div class=\"radio\"><label><input type=\"radio\" name=\"" + schermnaam + "\" value=\"" + optiereeks[C] + "\"/>" + optiereeks[C] + "</label></div><!--class=\"radio\"-->";
			}
			C=undefined; CC=undefined;			
		}else
		if(schermtype == "button")
		{
			uit += "\n<input type=\"button\" class=\"formcontrol btn btn-block btn-default\"" + veldnaam +  vereist + " value=\"" +schermnaam+ "\"/>";
		}else
		if(schermtype == "submit")
		{
			uit += "\n<input type=\"submit\" class=\"formcontrol btn btn-block btn-default\"" + veldnaam +  vereist + " value=\"" +schermnaam+ "\"/>";
		}else
		{
			uit += "\n<input type=\"" + schermtype + "\"" + veldnaam + formcontrol + vereist + " />";
		}

		uit += "\n</div><!-- class=\"" + veldcol + "\" -->";		

		uit += "\n</div><!-- class=\"row form-group\" -->";
		
		return uit;
	} 
	
	function formuleer(formuliernaam, schermnamen, labelcols, veldcols, vereisten, schermtypen, optiereeks)
	{
		var uit="";
		uit += "<form name=\"" + formuliernaam + "\" class=\"form-horizontal\" role=\"form\">";
		//uit += window.controls;
		var LL = schermnamen.length; for(var L=0; L < LL; L++)
		{
			uit += "\n"+ fcontrols(schermnamen[L], labelcols[L], veldcols[L], vereisten[L], schermtypen[L], optiereeks[L]) + "\n";
		}	
		
		uit += "\n<div class=\"row form-group\">"
			+ "\n<label class=\"control-label col-sm-5 col-md-4 col-lg-3\">"
			+ "\n</label>"
			+ "\n<div class=\"col-sm-7 col-md-8 col-lg-9\">"
			+ "\n<input type=\"submit\" class=\"form-control\">"
			+ "\n"
			+ "\n</div><!-- class=\"col-sm-7 col-md-8 col-lg-9\" -->"
			+ "\n</div><!-- class=\"row form-group\" -->"
			+ "\n";
			
		uit += "\n</form>";
		
		return uit;
	}

var bootstrapped_form_string = "<h3>" + tabel + "</h3>" + formuleer(tabel, schermnamen, labelcols, veldcols, vereist, schermtypen, opties);
document.getElementById('bootstrapped_form').value = HTMLfromCharCode(bootstrapped_form_string);
document.getElementById('bootstrapped_toon').innerHTML = document.getElementById('bootstrapped_form').value;//formuleer(tabel, schermnamen, labelcols, veldcols, vereist, schermtypen, opties);
</script>

<?php
//echo($actionscript);
echo($OOPactionscript);

echo($uitvoer);
?>

<h2>php class generator</h2>
<a href="noop/classMaker.php?class=<?= $_GET['tabel'] ;?>&velden=<?= $_GET['velden'];?>" target="_blank">generate and edit class code</a>
<?php
//require "oopClassGeneration.php";
?>

<h2>JS object constructor</h2>
function <?php echo $tabelnaam; ?>(<?php echo $tabelnaam; ?>Rec, formid, <?php echo implode(", ", $velden); ?>)
<br/>{
<br/>this.<?php echo $tabelnaam; ?>Rec = <?php echo $tabelnaam; ?>Rec;
<br/>this.formid = formid;
<?php
foreach($velden as $var)
	{
	echo "\n<br/>this.".$var." = ".$var.";";
	}
?>
<br/>}

<h2>php generating JS object array</h2>
$Qt="SELECT * FROM `<?php echo $tabelnaam; ?>`;";
$uitvoer="var <?php echo $tabelnaam; ?>Instance = new Array();";
$Rt= $db -> query($Qt);
while($St = $Rt -> fetch(PDO::FETCH_ASSOC))
{
$uitvoer.="<?php echo $tabelnaam; ?>Instance[<?php echo $tabelnaam; ?>Instance.length] = new <?php echo $tabelnaam; ?>(";
foreach($St as $key=>$val)
	{
<!--	$uitvoer.=$key;
	$uitvoer.=": \"";-->
	$uitvoer.="\"";
	$uitvoer.=$val."\", ";
	}
$uitvoer = substr($uitvoer, 0, -2);
$uitvoer.="), ";
}
$uitvoer = substr($uitvoer, 0, -2);
$uitvoer.="";

<h2>as direct JSON instances to embed in php script</h2>
$Qt="SELECT * FROM `<?php echo $tabelnaam; ?>`;";
$uitvoer="[";
$Rt= $db -> query($Qt);
while($St = $Rt -> fetch(PDO::FETCH_ASSOC))
{
$uitvoer.="{";
foreach($St as $key=>$val)
	{
	$uitvoer.=$key.": \"".$val."\", ";
	}
$uitvoer = substr($uitvoer, 0, -2);
$uitvoer.="}, ";
}
$uitvoer = substr($uitvoer, 0, -2);
$uitvoer.="]";
