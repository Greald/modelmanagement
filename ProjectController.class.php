<?php
require_once "ProjectModel.class.php";

class ProjectController extends ProjectIni
{

	//properties

	// 'dataMember PhpScript'
	protected $mPhpScript = "";
	// PhpScript-property accessor / mutator alias 'getter and setter'
	public function getPhpScript(){return $this->mPhpScript;}
	//Collection
	public function addPhpScriptElement($PhpScriptElement)
	{
		// Business rule
		if (is_array($this->mPhpScript))
		{
		}
		else
		{
			$this->mPhpScript = [];
		}
		$this->mPhpScript[] = $PhpScriptElement;
	}

	public function setPhpScript($value){return $this->addPhpScriptElement($value);}

	public function removePhpScriptElement($PhpScriptElement)
	{
		foreach($this->mPhpScript as $list => $pal)
		{
			if($pal == $PhpScriptElement)
			{
				unset($this->mPhpScript[$list]);
			}
		}
	}

	function InputPhpScriptCollection($listArr)
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			if(!is_array($listArr)){$listArr = [$listArr];}
			foreach($listArr as $elm)
			{
				$this->addPhpScriptElement($elm);
			}
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 



	// Constructor
//	 public function __construct($PhpScript)
//	 {
//	     $this->setPhpScript($PhpScript);
//	 }

	//methods
	// $projectElementsArr = [[$tablename, $ElementName, $Formid, $Fieldtype, $Fieldlength], ...]
	public function RegisterTable($projectElementsArr)
	{ 
		$DB = null; // only intentionally establish database connection here!
		$DB = new dbConnect();
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			$uit = "\n<br/>".self::class."->".__FUNCTION__."(..)".__LINE__."\n<br/>";
			$formid = time();
			foreach($projectElementsArr as $FeaturesArr)
			{
				if(preg_match('/^[0-9]{1,10}/', $FeaturesArr[2]) != 1)
				{
					$FeaturesArr[2] = $formid;
				}
				$uit .= ($ProjectModel = new ProjectModel($FeaturesArr[0]))->Create($FeaturesArr[2], $FeaturesArr[1], $FeaturesArr[3], $FeaturesArr[4]);
				$uit .= "\n<br/>";
			} 
			$ProjectModel = NULL;
			return $uit;		
		} 
		 catch (\PDOException $ex) 
		 { 
		 return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		 } 
		 
		 catch (\Exception $ex) 
		 { 
		 return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		 } 
		 
		 finally //close database connection 
		 { 
		 $DB = null; 
		 } 
	} 


	// create table in database
	public function TableMaker($tablename)
	{ 
		$DB = null; // only intentionally establish database connection here!
		$DB = new dbConnect();
		try 
		{ 
			 /* 
			 your code here 
			 using: $this->getProperty(), 
			 NOT: $this->mProperty 
			 for reasons of Encapsulation! 
			 */
			 $uit = self::class."->".__FUNCTION__."(..)".__LINE__.":\n<br/>";
			 
			 $newtable = ($ProjectModel = new ProjectModel($tablename))->Retrieve(" WHERE `tablename` = '".parent::PROJECTMODEL_PREFIX.$tablename."' ");
			 //echo self::class."->".__FUNCTION__."(..)".__LINE__.":\n<br/>newtable\n<br/>"; print_r($newtable);
			 $uit .= "\n<br/>".self::class."->".__FUNCTION__."(..)".__LINE__."\n<br/>".print_r($newtable,TRUE);
			 $uit .= "\n<br/>";
			 $uit .= $ProjectModel->RetrieveSource($newtable);
			 $uit .= $ProjectModel->RetrieveSQL($newtable); 
			 $uit .= "\n<br/>";

			 $XCreate = "CREATE TABLE IF NOT EXISTS `". $newtable[0]['tablename']."` (
				`".$tablename."Rec` INT(8) AUTO_INCREMENT PRIMARY KEY,
				`formid` INT(10), ";
			 $Velden = "";
			 foreach($ProjectModel->RetrieveResult($newtable) as $RecordArr)
			 {
			 	$XCreate .= "`".$RecordArr['fieldname']."` ".$RecordArr['fieldtype']." (".$RecordArr['fieldlength']."),";
			 	$Velden .= $RecordArr['fieldname'].",";
			 }
			 $XCreate = substr($XCreate, 0, -1)." );"; // delete last comma
			 $Velden = substr($Velden, 0, -1);  // delete last comma
			 $DB->exec($XCreate);
			 
			 $uit .= "\n<br/>".self::class."->".__FUNCTION__."(..)".__LINE__."\n<br/>".$XCreate;
			 $uit .= "\n<br/>";
			 
			 $uit .= "classMaker\n<br/><a href=\"noop/classMaker.php?class=".$newtable[0]['tablename']."&velden=".$Velden."\" target=\"_blank\">noop/classMaker.php?class=".$newtable[0]['tablename']."&velden=".$Velden."</a>";
			 $uit .= "\n<br/>";

			 return $uit;
		} 
		 catch (\PDOException $ex) 
		 { 
		 return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		 } 
		 
		 catch (\Exception $ex) 
		 { 
		 return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		 } 
		 
		 finally //close database connection 
		 { 
		 $DB = null; 
		 } 
	}

	
	public function PageOut($phpSnippetsArr)// array van php-scripts op volgorde van uitvoering
	{ 
		$DB = null; // only intentionally establish database connection here!
		try 
		{ 
			/* 
			your code here 
			using: $this->getProperty(), 
			NOT: $this->mProperty 
			for reasons of Encapsulation! 
			*/
			$this->InputPhpScriptCollection($phpSnippetsArr);
			$page="";
			foreach($this->getPhpScript() as $phpSnippet)
			{
				$page .= $phpSnippet;
			} 
			
			// de externe teijdelijke file-hack
			$tmpfname = @tempnam("./tmp", "hulpfile");
			$handle = fopen($tmpfname, "w");
			fwrite($handle, $page);
			fclose($handle);

			// !!!!!!!!!!!!!!!!!!!!!!!!! UITVOEREN !!!!!!!!!!!!!!
			// parse en stuur de php-code daarmee naar de client: 
			require $tmpfname; 
			// na gebruik weer verwijderen:
			unlink($tmpfname); 
			return ["log" => $tmpfname ." verwijderd. "];
		} 
		catch (\PDOException $ex) 
		{ 
			return "Er is een databasefout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		catch (\Exception $ex) 
		{ 
			return "Er is een fatale systeemfout in ".__CLASS__." ".__METHOD__.": " . $ex->getMessage(); 
		} 

		finally //close database connection 
		{ 
			$DB = null; 
		} 
	} 

	public function dispose(){/* your dispose function code here */}
}

if(!class_exists('clsExceptionInvalidPhpScript'))
	{class clsExceptionInvalidPhpScript extends \Exception {};}
?>